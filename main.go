package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"

	"crypto/tls"

	logrustash "github.com/bshuster-repo/logrus-logstash-hook"
	"github.com/sirupsen/logrus"
	ginlogrus "github.com/toorop/gin-logrus"
)

// Ref: https://stackoverflow.com/a/38548555
type bodyLogWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

func (w bodyLogWriter) Write(b []byte) (int, error) {
	w.body.Write(b)
	return w.ResponseWriter.Write(b)
}

func ginBodyLogMiddleware(c *gin.Context) {
	blw := &bodyLogWriter{body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
	c.Writer = blw
	c.Next()
	Ctx.Info("Response body: " + blw.body.String())
}

func echoGet(c *gin.Context) {
	c.JSON(http.StatusOK, c.Request.URL.Query())
}

func echoPostPutPatchDelete(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)

	if err != nil {
		Ctx.Error(err)
	}

	var requestContent map[string]interface{}
	json.Unmarshal([]byte(body), &requestContent)

	fmt.Println(body)

	c.JSON(http.StatusOK, requestContent)
}

var Ctx *logrus.Entry

func main() {
	log := logrus.New()
	conn, err := tls.Dial("tcp", "4094640e-3ed5-4480-bc09-b59cab5bf49b-ls.logit.io:10464", &tls.Config{RootCAs: nil})
	if err != nil {
		log.Fatal(err)
	}
	hook := logrustash.New(conn, logrustash.DefaultFormatter(logrus.Fields{"type": "myappName"}))
	log.Hooks.Add(hook)
	Ctx = log.WithFields(logrus.Fields{
		"method": "main",
	})
	r := gin.Default()
	r.Use(ginlogrus.Logger(log), gin.Recovery())
	r.Use(ginBodyLogMiddleware)

	echoAPI := r.Group("")
	{
		echoAPI.GET("", echoGet)
		echoAPI.POST("", echoPostPutPatchDelete)
		echoAPI.PUT("", echoPostPutPatchDelete)
		echoAPI.PATCH("", echoPostPutPatchDelete)
		echoAPI.DELETE("", echoPostPutPatchDelete)
	}

	Ctx.Info("App is running")

	r.Run(":8080")
}
